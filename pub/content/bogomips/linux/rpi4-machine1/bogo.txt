Linux PITV 5.4.72-v7l+ #1356 SMP Thu Oct 22 13:57:51 BST 2020 armv7l GNU/Linux
processor	: 0
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 270.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 1
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 270.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 2
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 270.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 3
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 270.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

Hardware	: BCM2711
Revision	: c03114
Serial		: 1000000004175d94
Model		: Raspberry Pi 4 Model B Rev 1.4
===========
BogoMIPS	: 270.00
BogoMIPS	: 270.00
BogoMIPS	: 270.00
BogoMIPS	: 270.00
===========
[    0.000000] CPU: ARMv7 Processor [410fd083] revision 3 (ARMv7), cr=30c5383d
5.4.72+
5.4.72-v7+
5.4.72-v7l+
5.4.72-v8+
MemTotal:        3737628 kB
MemFree:         2396392 kB
MemAvailable:    2994752 kB
Buffers:           43860 kB
Cached:           794584 kB
SwapCached:            0 kB
Active:           717068 kB
Inactive:         457148 kB
Active(anon):     400904 kB
Inactive(anon):    71712 kB
Active(file):     316164 kB
Inactive(file):   385436 kB
Unevictable:       64692 kB
Mlocked:              96 kB
HighTotal:       3080192 kB
HighFree:        1862780 kB
LowTotal:         657436 kB
LowFree:          533612 kB
SwapTotal:        102396 kB
SwapFree:         102396 kB
Dirty:              3436 kB
Writeback:             0 kB
AnonPages:        400460 kB
Mapped:           244900 kB
Shmem:            136840 kB
KReclaimable:      25128 kB
Slab:              49644 kB
SReclaimable:      25128 kB
SUnreclaim:        24516 kB
KernelStack:        2560 kB
PageTables:        12712 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:     1971208 kB
Committed_AS:    2144456 kB
VmallocTotal:     245760 kB
VmallocUsed:        6508 kB
VmallocChunk:          0 kB
Percpu:              464 kB
CmaTotal:         262144 kB
CmaFree:          241252 kB



deb http://raspbian.raspberrypi.org/raspbian/ buster main 
# Uncomment line below then 'apt-get update' to enable 'apt-get source'
#deb-src http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi
