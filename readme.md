


# rpi3b vs pinebook pro

````
BogoMIPS        : 38.40
BogoMIPS        : 38.40
BogoMIPS        : 38.40
BogoMIPS        : 38.40
````

````
BogoMIPS        : 48.00
BogoMIPS        : 48.00
BogoMIPS        : 48.00
BogoMIPS        : 48.00
BogoMIPS        : 48.00
BogoMIPS        : 48.00
````



# DELL

````
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
bogomips	: 4608.00
````


# Raspberry PI, PI 4 (rpi4)


My PI4:
````
processor	: 0
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 108.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 1
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 108.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 2
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 108.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 3
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 108.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

Hardware	: BCM2711
Revision	: c03114
Serial		: 1000000004175d94
Model		: Raspberry Pi 4 Model B Rev 1.4

````


root@PI4:/home/pi# cat /sys/devices/system/cpu/cpufreq/policy0/cpuinfo_max_freq 
1500000

````
Broadcom BCM2711, Quad core Cortex-A72 (ARM v8) 64-bit SoC @ 1.5GHz
1GB, 2GB, 4GB or 8GB LPDDR4-3200 SDRAM (depending on model)
2.4 GHz and 5.0 GHz IEEE 802.11ac wireless, Bluetooth 5.0, BLE
Gigabit Ethernet
2 USB 3.0 ports; 2 USB 2.0 ports.
Raspberry Pi standard 40 pin GPIO header (fully backwards compatible with previous boards)
2 × micro-HDMI ports (up to 4kp60 supported)
2-lane MIPI DSI display port
2-lane MIPI CSI camera port
4-pole stereo audio and composite video port
H.265 (4kp60 decode), H264 (1080p60 decode, 1080p30 encode)
OpenGL ES 3.1, Vulkan 1.0
Micro-SD card slot for loading operating system and data storage
5V DC via USB-C connector (minimum 3A*)
5V DC via GPIO header (minimum 3A*)
Power over Ethernet (PoE) enabled (requires separate PoE HAT)
Operating temperature: 0 – 50 degrees C ambient
* A good quality 2.5A power supply can be used if downstream USB peripherals consume less than 500mA in total.
````


````
 cat /proc/cpuinfo | grep ogo  >

BogoMIPS	: 108.00
BogoMIPS	: 108.00
BogoMIPS	: 108.00
BogoMIPS	: 108.00


pi@PI4:~ 
<CMD:    cat /proc/cpuinfo | grep ogo  >
BogoMIPS        : 270.00
BogoMIPS        : 270.00
BogoMIPS        : 270.00
BogoMIPS        : 270.00

[    0.000000] Linux version 5.10.103-v7l+ (dom@buildbot) (arm-linux-gnueabihf-gcc-8 (Ubuntu/Linaro 8.4.0-3ubuntu1) 8.4.0, GNU ld (GNU Binutils for Ubuntu) 2.3
4) #1529 SMP Tue Mar 8 12:24:00 GMT 2022
[    0.000000] CPU: ARMv7 Processor [410fd083] revision 3 (ARMv7), cr=30c5383d
[    0.000000] CPU: div instructions available: patching division code
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, PIPT instruction cache
[    0.000000] OF: fdt: Machine model: Raspberry Pi 4 Model B Rev 1.4


[    0.000000] Memory: 3601140K/4050944K available (10240K kernel code, 1366K rwdata, 3196K rodata, 2048K init, 882K bss, 122124K reserved, 327680K cma-reserved, 3264512K highmem)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
````

# PineBook 64


````
4 x ARM Cortex A53 cores @ 1.152 GHz
ARM Mali 400 MP2 GPU
2GB LPDDR3 RAM
1080p IPS panel
Bootable Micro SD slot
16GB of eMMC (upgradable)
HD digital video out
2x USB 2.0 Host
Lithium polymer battery (10000mAH)
Stereo speakers
802.11 b/g/n WiFi with Bluetooth 4.0
3.5mm headphone jack
Microphone
0.3 MP front-facing camera
Full-sized keyboard
Large trackpad
3.5mm OD/1.35mm ID (5V 3A) port

11.6″ PINEBOOK LINUX LAPTOP [Reserved For “Reduce Digital Gap Campaign”]
Community price: $99.99

CPU: 1.2GHz 64-Bit Quad-Core ARM Cortex A53
RAM: 2 GB LPDDR3 RAM Memory
Flash: 16 GB eMMC 5.0 (upgradable up to 64GB)
Wireless: WiFi 802.11bgn + Bluetooth 4.0
USB 2.0 Port: 2
MicroSD Card Slot: 1
Mini Digital Video Port: 1
Headphone Jack: 1
Microphone: Built-in
Keyboard: Full Size Keyboard
Touch-pad: Large Multi-Touch Touchpad
Power: Input: 100~240V, Output: 5V3A
Battery: Lithium Polymer Battery (10000mAH)
Display: 11.6″ IPS LCD (1920 x 1080)
Front Camera: 0.3 Megapixels
Power Supply included
Dimension: 299mm x 200mm x 12mm (WxDxH)
Weight: 1.04 kg (2.30 lbs)
Warranty: 30 days
````

PINEBOOK is an 11.6″ notebook powered by the same quad-core ARM

123mm x 115mm x 48mm; Weight of 543 grams

weight: 

Large 10,000 mAh battery to give all day battery life



slarm64 (unofficial slackware) Pinebook Pro RK3399 (aarch64)


# 14″ PINEBOOK Pro LINUX LAPTOP (ANSI. US Keyboard)

````
€ 652,29

CPU: 64-Bit Dual-Core ARM 1,8 GHz Cortex A72 und Quad-Core ARM 1,4 GHz Cortex A53
GPU: Quad-Core MALI T-860
RAM: 4 GB LPDDR4 Dual Channel System DRAM Speicher
Flash: 64 GB eMMC 5,0
Wireless: WiFi 802,11 AC + Bluetooth 5,0
Eine USB 3,0 und ein USB 2,0 Typ-EINE Host Ports
USB 3,0 Typ-C ports mit alt-modus display heraus (DP 1.2) und 15W 5V 3A ladung.
MicroSD Card Slot: 1
Kopfhörer Jack: 1
Mikrofon: Built-in
Tastatur: Volle Größe ANSI (UNS) typ Tastatur
Touch-pad: Große Multi-Touch Touchpad
Power: Eingang: 100 ~ 240V, Ausgang: 5V3A
Batterie: Lithium-Polymer-Batterie (9600mAH)
Display: 14.1 ″ IPS LCD (1920 x 1080)
Vordere Kamera: 2,0 Megapixel
Netzteil enthalten, kommt mit sowohl UNS und EU stecker
Dimension: 329mm x 220mm x 12mm (Bxtxh)
1,26 kg (2,78 £)

Pinebook Pro orders will dispatch weekly from Hong Kong 3PL (Third Party Logistic) warehouse.
CPU: 64-Bit Dual-Core ARM 1.8GHz Cortex A72 and Quad-Core ARM 1.4GHz Cortex A53
GPU: Quad-Core MALI T-860
RAM: 4 GB LPDDR4 Dual Channel System DRAM Memory
Flash: 64 GB eMMC 5.0
Wireless: WiFi 802.11AC + Bluetooth 5.0
One USB 3.0 and one USB 2.0 Type-A Host Ports
USB 3.0 Type-C ports with alt-mode display out (DP 1.2) and 15W 5V 3A charge.
MicroSD Card Slot: 1
Headphone Jack: 1
Microphone: Built-in
Keyboard: Full Size ANSI(US) type Keyboard
Touch-pad: Large Multi-Touch Touchpad
Power: Input: 100~240V, Output: 5V3A
Battery: Lithium Polymer Battery (9600mAH)
Display: 14.1″ IPS LCD (1920 x 1080)
Front Camera: 2.0 Megapixels
Power Supply included, comes with both US and EU plugs
Dimension: 329mm x 220mm x 12mm (WxDxH)
Weight: 1.26 kg (2.78 lbs)
Warranty: 30 days
````


# 11" 

A Thin and Light Open Source ARM 64-bit Notebook for Tinkering
The PINEBOOK is an 11.6″ notebook powered by the same quad-core ARM Cortex A53 64-Bit Processor used in our popular PINE A64 Single Board Computer. It is very lightweight, hackable and comes equipped with a full-sized keyboard and large multi-touch trackpad. It runs numerous mainline Linux distributions as well as *BSD and Android. The most recent iteration has received an upgrade in the form of a 1080p IPS panel. 


# Perfs

lscpu      
Architecture:          aarch64
Byte Order:            Little Endian
CPU(s):                6
On-line CPU(s) list:   0-5
Thread(s) per core:    1
Core(s) per socket:    3
Socket(s):             2
Model:                 4
Model name:            ARMv8 Processor rev 4 (v8l)
CPU max MHz:           1512.0000
CPU min MHz:           408.0000
BogoMIPS:              48.00
Flags:                 half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt lpae evtstrm aes pmull sha1 sha2 




lukasz@Debian-Desktop:cat /sys/devices/system/cpu/cpufreq/policy/cpuinfo_max_freq
1512000

lukasz@Debian-Desktop:cat /sys/devices/system/cpu/cpufreq/policy4/cpuinfo_max_freq
1992000


sudo lscpu      
Architecture:          aarch64
Byte Order:            Little Endian
CPU(s):                6
On-line CPU(s) list:   0-5
Thread(s) per core:    1
Core(s) per socket:    3
Socket(s):             2
Model:                 4
Model name:            ARMv8 Processor rev 4 (v8l)
CPU max MHz:           1512.0000
CPU min MHz:           408.0000
BogoMIPS:              48.00
Flags:                 half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt lpae evtstrm aes pmull sha1 sha2 crc32





Think about, rk3399 is about twice as fast as the rpi4b's bcm2711. i think your experience will depend on how much graphics/video you utilize for any small sbc-like computer.



